After the program reads in the command line argument, one process is created. This process
is then forked to create a child process which is then forked to create the grandchild. By 
analyzing the output before the processes are terminated, it can be seen that processes that 
are spawned subsequently, tend to have sequentual process ID's. 

After termination of the processes, some interesting behavior takes place. Suppose you ran the 
program with an argument of 1, this would spawn the processes and then insure that the grandchild 
is ended first followed by the child and lastly the parent. THe pid, ppid, gid and sid all stay 
the same. BUt where it gets interesting is when the argument is either 2 or 3. Let's look at the 
last run of the program; parent ends before child and grandchild. The pid, gid and sid are all 
what would be expected, by what changes is the ppid (parent process id). For the child and it's 
subprocess, this value is not set to 1. Why is that, you ask? Since the parent process was 
terminated, the child is now set to the first process... then gets terminated. This is why the 
grandchild also has a ppid of 1 in the output. 