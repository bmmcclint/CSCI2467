#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void genericProcInfoDisplay() {
  printf("PID: %i\n", getpid());
  printf("PPID: %i\n", getppid());
  printf("GID: %i\n", getgid());
  printf("SID: %i\n", getsid(getpid()));
}

void paProcInfoDisplay() {
  printf("\nParent Process Info: \n");
  genericProcInfoDisplay();
}

void chProcInfoDisplay() {
  printf("\nChild Process Info: \n");
  genericProcInfoDisplay();
}

void gcProcInfoDisplay() {
  printf("\nGrandchild Process Info: \n");
  genericProcInfoDisplay();
}

void procSpawner(int paSleep, int chSleep, int gcSleep) {
  pid_t paPID;
  pid_t chPID;
  pid_t gcPID;

  if ((paPID = fork()) < 0) {
    printf("\nCouldn't create child process... \n");
  }
  else if (paPID == 0) {
    paProcInfoDisplay();

    if ((chPID = fork()) < 0) {
      printf("\nCouldn't fork child process... \n");
    }

    else if (chPID == 0) {
      chProcInfoDisplay();

      if ((gcPID = fork()) < 0) {
        printf("\nCouln't create grandcild process... \n");
      }

      else if (gcPID == 0) {
        gcProcInfoDisplay();
        printf("\nEliminating processes from existence!\n");
        sleep(gcSleep);
        gcProcInfoDisplay();
        exit(0);
      }
      sleep(chSleep);
      chProcInfoDisplay();
      exit(0);
    }
    sleep(paSleep);
    paProcInfoDisplay();
    exit(0);
  }
}

void gcDeath() {
  printf("\nGrandchild gets aborted -> Child gets deleted -> Parent gets domed\n");
}

void chDeath() {
  printf("\nChild gets deleted -> Parent gets domed -> Grandchild gets aborted\n");
}

void paDeath() {
  printf("\nParent gets domed -> Grandchild gets aborted -> Child gets deleted\n");
}

main (int argc, char *argv[]) {
  if (argc != 2) {
    printf("You've entered to few/many arguments... try again.\n");
  }

  else {
    if (strcmp(argv[1], "1") == 0) {
      gcDeath();
      procSpawner(9, 6, 3);
      sleep(10);
      exit(0);
    }

    else if (strcmp(argv[1], "2") == 0) {
      chDeath();
      procSpawner(6, 3, 9);
      sleep(10);
      exit(0);
    }

    else if (strcmp(argv[1], "3") == 0) {
      paDeath();
      procSpawner(3, 6, 9);
      sleep(10);
      exit(0);
    }
  }
  exit(0);
}
