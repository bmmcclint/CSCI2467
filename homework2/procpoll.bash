#!/bin/bash

#constantly displays current processes for user until stopped

_user="$(id -u -n)"

while true; 
do 
	echo 'These processes belong to you - '$_user': '
 	ps -U $_user -u $_user u --forest
 	echo 'Hit CTRL+C to stop...' 
 	sleep 2
 	clear 
 done