#!/bin/bash

# prints out how many times a user has logged in to a machine

read -p "Enter the user you'd like to search for:" username

touch users.txt && who -au > users.txt && grep -ow $username users.txt | wc -l | tr -d '\n' && echo " - is the total number of logins" && rm users.txt