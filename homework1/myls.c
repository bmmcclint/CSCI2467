#include "apue.h"
#include <dirent.h>
#include <string.h>

char *currDir = NULL;
unsigned int j = NULL;
unsigned int i = NULL;
DIR *cdp;
struct dirent *dirp = NULL;
struct dirent **fileList;

int dirSort(const struct dirent **dir1, const struct dirent ** dir2) {
	return (strcasecmp((*(struct dirent **)dir1)->d_name,
		(*(struct dirent **)dir2)->d_name));
}

int currentDirLs(char *argv[]) {
	currDir = ".";
	if (currDir == NULL) {
		perror(currDir);
	}
	int j = scandir(".", &fileList, 0, dirSort);

	if (j < 0) {
		perror("scandir");
	}
	else if (argv[1]) {
		// if argument is -a display all files/directories
		if (!strcmp(argv[1], "-a")) {
			for (i = 0; i < j; i++) {
				printf("%s \t", fileList[i]->d_name);
				free(fileList[i]);
			}
		}
		// if argument is -r display files/directories in reverse order
		else if (!strcmp(argv[1], "-r")) {
			while (j--) {
				if (fileList[j]->d_name[0] != '.') {
					printf("%s \t", fileList[j]->d_name);
					free(fileList[j]);
					}
				}
			}
		}
		printf("\n");
	
	free(fileList);
}

int specificDir(char **argv) {
	j = scandir(argv[1], &fileList, 0, dirSort);

	if (j < 0) {
		perror("scandir");
	}

	else if (argv[1]) {
		// if second arguement is -a display all files
		if (!strcmp(argv[2], "-a")) {
			for (i = 0; i < j; i++) {
				printf("%s \t", fileList[i]->d_name);
				free(fileList[i]);
			}
		}
	
		// if second argument is -r display files in reverse order
	else if (!strcmp(argv[2], "-r")) {
		while (j--) {
			if (fileList[j]->d_name[0] != '.') {
				printf("%s \t", fileList[j]->d_name);
				free(fileList[j]);
				}
			}
		}
	}
	printf("\n");	
	free(fileList);
}

int main(int argc, char *argv[]) {
	// if 1 arg display current directory
	if (argc == 1) {
		cdp = opendir(".");
		j = scandir(".", &fileList, 0, dirSort);
		for (i = 0; i < j; i++) {
			if (fileList[i]->d_name[0] != '.') {
				printf("%s \t", fileList[i]->d_name);
				free(fileList[i]);
			}
		}
		printf("\n");
	}

	/* if 2 args display -a or -r for current directory specified directory
	 * or display files and directories for user supplied directory
	 */
	else if (argc == 2) {
		if ((strcmp(argv[1], "-a") == 0) || (strcmp(argv[1], "-r")) == 0) {
			currentDirLs(argv);
		}
		else {
			cdp = opendir(argv[1]);
			j = scandir(argv[1], &fileList, 0, dirSort);
			for (i = 0; i < j; i++) {
				if (fileList[i]->d_name[0] != '.') {
					printf("%s \t", fileList[i]->d_name);
					free(fileList[i]);
				}
			}
			printf("\n");
		}
	}

	// if 4 args display specified dir with -a or -r
	else if (argc == 3) {
		j = scandir(argv[1], &fileList, 0, dirSort);
		specificDir(argv);
	}

	// if more than 3 args display usage message
	else {
		printf("usage: myls [flag] or directory name [flag]\n");
	}
	exit(0);
}