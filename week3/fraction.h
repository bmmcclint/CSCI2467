#include <stdio.h>

struct Fraction {
    int numerator;
    int denominator;
};

void reduce(struct Fraction *myFrac);
