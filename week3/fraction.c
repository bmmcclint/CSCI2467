#include "./fraction.h"

int main(int argc, char *argv[])
{

    struct Fraction      aFraction;

    aFraction.numerator = 10;
    aFraction.denominator = 20;

    reduce(&aFraction);

    printf("%d / %d\n",aFraction.numerator, aFraction.denominator);

    return 0;
}

void reduce(struct Fraction *myFrac) {

    if (myFrac->denominator%myFrac->numerator == 0)
    {
       myFrac->denominator /= myFrac->numerator;
       myFrac->numerator /= myFrac->numerator;
    }
}
    
